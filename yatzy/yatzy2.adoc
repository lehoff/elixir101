:imagesdir: images

= Making an OTP application out of `yatzy`

At this point you have learnt what the Erlang process model is about and how to write both
a simple server process (`yatzy_player`) and a finite state machine(`yatzy_turn`).

In real life you would never write these components from scratch - you should be using the
`gen_server` and the `gen_statem` OTP behaviours to do the job.

== `yazty_player` as a `gen_server`

Chapter 12 "OTP Behaviours" has a sub-section on `gen_server` - read that and then convert
`yatzy_player` to a `gen_server`.

NOTE: the interface should not change, so the unit tests should still be good. Except that
`new/1` should be `start_link/1` as that would be the OTP way of naming the function used
to create a process.

== `yatzy_turn` as a `gen_statem`

Unfortunately, `gen_statem` has no nice write up available, so you have read the
documentation about it.

`gen_statem` has some pretty nice features, but the documentation is rather abstract, so
when you run into issues do ask a little faster than normally and we'll talk about how to
read the documentation. Everything is in the documentation, but not always in the most
approachable format.

== Add a supervisor for the `yatzy_player`

=== Only `john` can play

Start by changing the generated `yatzy_sup` to a `one_for_one` supervisor that starts one child process, namely a
`yatzy_player` called `john`.

Now, when you run `rebar3 shell` you should be able to start `observer`
(`observer:start()`) and see the supervision tree for the `yatzy` application.

Try poking a bit around in the `observer` and see what you can find.

=== Dynamic number of players

Now use dynamic children for the `yatzy_sup` so that more than one player can play.

Use `observer` to see what is going on.

== Add a way to start `yatzy_turn` under the `yatzy_sup` supervisor

Another dynamic child...





